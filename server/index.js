const express = require('express');
const { Nuxt, Builder } = require('nuxt')
const app = express()
const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 3000
const fs = require('fs');
const bodyParser = require('body-parser')
const multer = require('multer')
const UPLOAD_PATH = 'uploads';
const upload = multer({ dest: `${UPLOAD_PATH}/` });

var cookieParser = require('cookie-parser');
var passport = require('passport');
var Strategy = require('passport-facebook').Strategy;
const FacebookStrategy = require('passport-facebook');


app.set('port', port)

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }
  app.use(cookieParser('111'));
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(passport.initialize());

  // Give nuxt middleware to express
  passport.use(new FacebookStrategy({
    clientID: '1988439204772462',
    clientSecret: 'a8fa416f55c1c39802d2a51f267ed8c3',
    callbackURL: "http://localhost:3000/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    
    console.log(accessToken, refreshToken, profile, cb)
    
    return cb(null, profile,accessToken);

  }
));
passport.serializeUser(function(user, done) {
  done(console.log(user), user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

app.get('/auth/facebook',passport.authenticate('facebook'));

app.get('/auth/facebook/callback',passport.authenticate('facebook', { failureRedirect: '/login' }),
function(req, res, next) {

res.cookie('cookieName', req.authInfo)

res.redirect('/');
});
 app.get('/logout',function(req,res){
  res.clearCookie('cookieName')

  res.redirect('/');
 })
app.post('/upload',function(req,res){
 var data = req.body.id.replace(/^data:image\/\w+;base64,/, "");
 var buf = new Buffer(data, 'base64');
  fs.writeFile(`./uploads/${req.body.fileName}`,buf,function (err, data) {
    if (err) throw err;
    
  })
})


  app.use(nuxt.render)

  
  // Listen the server
  app.listen(port, host)
  console.log('Server listening on http://' + host + ':' + port) // eslint-disable-line no-console
}

start()
