FROM node:latest

WORKDIR /app

# copy only the package.json to take advantage of cached Docker layers
COPY . .

# install project dependencies
RUN npm install
RUN npm rebuild node-sass --force

# build for production with minification
RUN npm run build

# clean up (i.e. extract 'dist' folder and remove everything else)

EXPOSE 3000

CMD [ "npm","start" ]
# docker build -t tomlishan/uploadimg .
# docker push tomlishan/uploadimg
# docker run -d -it -p 3000:3000 -v /root/uploads:/app/uploads  d2f8f2bb2d3b