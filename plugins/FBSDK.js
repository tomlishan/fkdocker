window.fbAsyncInit = function () {
    FB.init({
      appId: '192246734675475',
      cookie: true,
      version: 'v2.12',
    });
    document.dispatchEvent(new Event('fb_init'));
  };
  
  (function (d, s, id) {
    let js,
      fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));